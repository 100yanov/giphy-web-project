/* eslint-disable max-len */
/* eslint-disable wrap-iife */
export let limit = 24;
export const changeLimit = (num) => {
  limit = num;
}
export const CONTAINER = '#gifs';
export const API_KEY = '?api_key=mJ2IZsKfV2NjXJr20ZgHYqi7YIdZdEyf';
export const UPLOAD_URL = 'http://upload.giphy.com/v1/gifs';
export const BASE_URL = 'https://api.giphy.com/v1/gifs';

export const templateHtml = (gif) => {
  $(CONTAINER).append(`
  <div class="img-container">
  <img class="giphy" gif-id="${gif.id}" src="${gif.images.fixed_width_downsampled.url}">
  <div class="titleAndLike">
      <button btn-id=${gif.id} class="button-like" type="button">
          <i class="fa fa-heart"></i>
      </button>
  </div>
</div>`);
}

// Function to manipulate localStorage key storing id's of favorite gifs
const favGifs = () => {
  const STORAGE_KEY = 'favoriteIds'; // Initialise localStorage key.
  const SEPARATOR = '-|-'; // Adding unique separator.
  const fromStorage = localStorage.getItem(STORAGE_KEY); // Gets localStorage value by its key
  const array = fromStorage && fromStorage.split(SEPARATOR) || []; // If fromStorage is null assigns empty array to it, else splits the array by the unique separator..
  // Adds gif's id as value to localStorage's key using setItem(key:value).
  const addId = (id) => {
    array.push(id);
    localStorage.setItem(STORAGE_KEY, array.join(SEPARATOR));
    return array;
  }
  // Removes gif's id from localStorage's key using setItem(key:value).
  const removeId = (id) => {
    const currentFavIndex = array.indexOf(id);
    array.splice(currentFavIndex, 1);
    localStorage.setItem(STORAGE_KEY, array.join(SEPARATOR));
    return array;
  }
  // Returns copy of the original array to make sure the user won't manipulate it.
  const getItems = () => array.slice();
  // Checks if GIF id is already stored in the localStorage's key.
  const hasItem = (gifId) => array.includes(gifId);

  return {
    addId,
    removeId,
    getItems,
    hasItem
  }
}

// Passes uploaded gif's id to localStorage.
const uploadedGifs = () => {
  const STORAGE_KEY = 'uploadedIds'; // Initialise localStorage key.
  const SEPARATOR = '-|-' // Adding unique separator.
  const fromStorage = localStorage.getItem(STORAGE_KEY); // Gets localStorage value by its key
  const array = fromStorage && fromStorage.split(SEPARATOR) || []; // If fromStorage is undefined assigns empty array to it, else splits the array by unique separator.
  // Adds gif's id as value to localStorage's key using setItem(key:value).
  const addId = (id) => {
    array.push(id);
    localStorage.setItem(STORAGE_KEY, array.join(SEPARATOR));
    return array;
  }
  const getItems = () => array.slice(); // Returns copy of the original array to make sure the user won't manipulate it.
  return {
    addId,
    getItems,
  }
}

// Initialize a variable(storage) and using closure we provide said variable with the favGifs's functionality.
export const storage = favGifs();
// Initialize a variable(uploaded) and using closure we provide said variable with the uploadedGifs's functionality.
export const uploaded = uploadedGifs();
