/* eslint-disable max-len */
export const searchGif = (callback) => $(document).on('click', '#btnSearch', callback);

export const trending = (callback) => $(document).on('click', '#trending', callback);

export const titleAndLike = (callback) => $(document).on('mouseenter', '.giphy', callback);

export const upload = (callback) => $(document).on('click', '#upload-btn', callback);

export const addFavorite = (callback) => $(document).on('click', '.button-like', callback);

export const diplayFavorites = (callback) => $(document).on('click', '#favorites', callback);

export const displayUploaded = (callback) => $(document).on('click', '#myUploads', callback);

export const enterSearch = (callback) => $(document).on('keypress', '#search', callback);

export const displayedGifsNumber = (callback) => $(document).on('change', '.options', callback);
