/* eslint-disable no-undefined */
/* eslint-disable no-alert */
/* eslint-disable max-len */
import {
  limit,
  CONTAINER,
  API_KEY,
  UPLOAD_URL,
  storage,
  uploaded,
  changeLimit,
  BASE_URL,
  templateHtml,
} from './common.js'


/**
* Displays Trending Gifs
*
* Function which is passed as a callback to trending.
* Gets the trending gifs from APIs endpoint.
* Appends the result to the container.
*/

export const showTrending = async (ev) => {
  $(CONTAINER).empty();
  try {
    const response = await fetch(`${BASE_URL}/trending${API_KEY}&limit=${limit}`);
    const gifs = await response.json();
    gifs.data.forEach(templateHtml);
  } catch (error) {
    alert(error.message);
  }
};

/**
* Displays Searched Gifs
*
* Function which is passed as a callback to searchGif.
* Gets the searched gifs from APIs endpoint by using the input keywords.
* Appends the result to the container.
*/

export const searchFor = async (ev) => {
  if ($('#search').val() === '') {
    return;
  }
  const $searchedKeyWord = $('#search').val();
  try {
    const resp = await fetch(`${BASE_URL}/search${API_KEY}&limit=${limit}&q=${$searchedKeyWord}`);
    const srchResult = await resp.json();
    if (srchResult.data.length === 0) {
      alert('No GIFs found, please search again')
      $('#search').val('');
      return;
    }
    $(CONTAINER).empty();
    await srchResult.data.forEach(templateHtml);
  } catch (err) {
    alert(err.message);
  }
  $('#search').val('');
};

/**
* Displays GIF's like button and title on hover
*
* Checks if GIF has title already.
* If it doesn't, get's the gif title from API and appends it.
*/

export const showTitleAndLike = async (ev) => {
  if ($(ev.target).siblings('div').children('label').length === 0) {
    const target = $(ev.target).attr('gif-id');
    try {
      const response = await fetch(`${BASE_URL}/${target}${API_KEY}`);
      const result = await response.json();
      $(ev.target).siblings('div').prepend(`
      <label id="gif-title">${await result.data.title}</label>`);
    } catch (err) {
      alert(err);
    }
  }
};

/**
* Uploads GIF from user's file system to giphy channel
*/

export const uploadGif = async (ev) => {
  ev.preventDefault();
  const file = await $('#select-file')[0].files[0]; // Assigns the gif we want to upload to file variable.
  if (file === undefined) {
    alert(`Please choose file`);
    return;
  }
  const formData = new FormData();
  formData.append('file', file);
  try {
    const upload = await fetch(`${UPLOAD_URL}${API_KEY}`, { // Uploads selected gif to giphy channel.
      method: 'POST',
      body: formData,
    });
    const isUploaded = await upload.json();
    const uploadedId = await isUploaded.data.id; // Assigns the id of the uploded gif to uploadedId variable.
    uploaded.addId(uploadedId); // Ads uploadedId to uploadId key in localStorage.
    $('#success').html('✔')
    setTimeout(() => {
      $('#success').empty();
    }, 3000);
    // alert('You have successfully uploaded your GIF!');
  } catch (err) {
    alert(err);
  }
};

/**
* Adds Favorited Gif's Id to the Favorites List('favoriteIds') in the localStorage.
*
* Function which is passed as a callback to addFavorite.
* Checks if Gif's Id is already stored in the favoriteIds array.
* If the Id is not the array it adds the Id, else it removes the Gif's Id from the array.
*/

export const addFavGif = (ev) => {
  const favgifId = $(ev.currentTarget).parent('.titleAndLike').siblings('.giphy').attr('gif-id');
  if (storage.hasItem(favgifId) === false) {
    storage.addId(favgifId);
  } else {
    storage.removeId(favgifId);
  }
};

/**
* Displays Favorites Gifs
*
* Function which is passed as a callback to displayFavorites.
* Stingifies the value of the uploaded key of localStorage.
* Gets the favorited gifs from API by their gif keys.
* Appends the result to the container.
*/

export const favoritesDisplay = async (ev) => {
  ev.preventDefault();
  $(CONTAINER).empty();
  let res = [];
  try {
    if (storage.getItems().length === 0) {
      alert(`You haven't added any GIFs to you Favorites! Enjoy a random GIF!`);
      const random = await fetch(`${BASE_URL}/random${API_KEY}`);
      res = [(await random.json()).data];
    } else {
      const allFavGifs = storage.getItems().join(',');
      const response = await fetch(`${BASE_URL}${API_KEY}&ids=${allFavGifs}`);
      res = [...(await response.json()).data];
    }
    res.forEach(templateHtml);
  } catch (err) {
    alert(err);
  }
};

/**
* Displays Uploaded Gifs
*
* Function which is passed as a callback to displayUploaded.
* Stingifies the value of the uploaded key of localStorage.
* Gets the uploaded gifs from API by their gif keys.
* Appends the result to the container.
*/

export const uploadedDisplay = async (ev) => {
  ev.preventDefault();
  $(CONTAINER).empty();
  const allFavGifs = uploaded.getItems().join(',');
  try {
    const response = await fetch(`${BASE_URL}${API_KEY}&ids=${allFavGifs}`);
    const res = await response.json();
    await res.data.forEach(templateHtml);
  } catch (err) {
    alert(err);
  }
};

// Triggers click event on #btnSearch when users press "Enter"
export const pressEnter = (e) => {
  if (e.which === 13) {
    e.preventDefault();
    $('#btnSearch').click();
  }
}

// Listens if the value of the dropdown menu is changed
export const changeNumberGifs = () => {
  const selected = $('.options option:selected').val();
  changeLimit(selected);
}

export const appendNumToDropDown = () => {
  const $select = $('#1-30');
  for (let i = 4; i <= 30; i = i + 4) {
    if (i === 24) {
      $select.append($(`<option selected="selected" value="${i}"></option>`).val(i).html(i))
    } else {
      $select.append($(`<option value="${i}"></option>`).val(i).html(i))
    }
  }
}
