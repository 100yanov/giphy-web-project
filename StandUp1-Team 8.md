# Stand Up - 06.02.2020 - Team 8

- **Positive**
<br/>
1. We have completed all the tasks that were required from us.

2. Communication in the team was excellent.
3. Everyone took their job seriously and we helped each other with any inquiries.

<br/>

- **Improve**
<br/>

1. We could have done the GitLab board issues organization a bit better and we could have appointed the task in the proper manner of their development. 
2. We could have organized our files from the beginning of the project.

- **Negative** 
<br/>

1. There were no significant negative aspects in our teamwork.
<br/>