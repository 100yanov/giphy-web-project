import {
  trending, searchGif, titleAndLike, upload, addFavorite, diplayFavorites,
  displayUploaded, enterSearch, displayedGifsNumber
}
  from './scripts/events.js'

import {
  showTrending, searchFor, showTitleAndLike,
  uploadGif, addFavGif, favoritesDisplay, uploadedDisplay,
  pressEnter, changeNumberGifs, appendNumToDropDown
}
  from './scripts/view.js'

// Show trending by default
showTrending();

// Show trending
trending(showTrending);

// Show search result
searchGif(searchFor);

// Show title and like on hover
titleAndLike(showTitleAndLike);

// Add GIF to the favorites list
addFavorite(addFavGif);

// Upload GIF to the upload list
upload(uploadGif);

// Display favorites list
diplayFavorites(favoritesDisplay);

// Display uploaded
displayUploaded(uploadedDisplay);

// Press Enter to search for GIFs
enterSearch(pressEnter);

// Select how many GIFs to be displayed
displayedGifsNumber(changeNumberGifs);

appendNumToDropDown();
